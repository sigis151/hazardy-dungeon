using UnityEngine;
using System.Collections;

public static class RoomSelectionPool
{
	private static bool[] active = new bool[Constant.ROOM_COUNT];
	private static int lastPickedType = -1;

    public static Room getRoom(int id, GameObject parent)
    {
        Object prefab = Resources.Load("prefabs/Room");
        Room temp = (GameObject.Instantiate(prefab) as GameObject).GetComponent("Room") as Room;
        temp.transform.SetParent(parent.transform);
        temp.transform.position = parent.transform.position;
        if (id < 0 || id >= Constant.ROOM_COUNT)
            return null;
        switch (id)
        {
            case 0:
                temp.Set("Snakepit", 0.1f, 1);
                break;
            case 1:
                temp.Set("Fire", 0.2f, 0);
                break;
            case 2:
                temp.Set("Acid", 0.2f, 1);
                break;
            case 3:
                temp.Set("Spikes", 0.25f, 2);
                break;
            case 4:
                temp.Set("Shock", 0.15f, 6);
                break;
            case 5:
                temp.Set("Quicksand", 0.1f, 5);
                break;
            case 6:
                temp.Set("Lava", 0.4f, 0);
                break;
            case 7:
                temp.Set("Swinging blades", 0.2f, 2);
                break;
            case 8:
                temp.Set("Arrows", 0.1f, 2);
                break;
            case 9:
                temp.Set("Slackline", 0.5f, 4);
                break;
            case 10:
                temp.Set("Deep water", 0.15f, 5);
                break;
            case 11:
                temp.Set("Poisonous gas", 0.3f, 1);
                break;
            case 12:
                temp.Set("Stone rain", 0.1f, 3);
                break;
        }
        return temp;
    }
	
	public static Room getRandom(GameObject parent)
	{
		
		/*if (getRemainingPoolItemCount() < Constant.MIN_ROOM_POOL_ITEMS)
			resetPool();
		int randomNumber = -1;
		while (randomNumber < 0 || randomNumber >= Constant.ROOM_COUNT)
		{
			// generate new number between 0 and Constant.ROOM_COUNT-1
			randomNumber = UnityEngine.Random.Range(0, Constant.ROOM_COUNT - 1);


            if (!active[randomNumber] || getRoom(randomNumber, parent).getDamageType() == lastPickedType)
				randomNumber = -1;
		}
        lastPickedType = getRoom(randomNumber, parent).getDamageType(); // save the last picked item, disallow same damage type next pick
		active[randomNumber] = false; // remove from the pool
        */
        return getRoom(Random.RandomRange(0, 13), parent);
	}
	
	public static void resetPool()
	{
		for (int i = 0; i < Constant.ROOM_COUNT; i++)
			active[i] = true;
	}
	
	public static void resetLastPicked()
	{
		lastPickedType = -1;
	}
	
	private static int getRemainingPoolItemCount()
	{
		int count = 0;
		for (int i = 0; i < Constant.ROOM_COUNT; i++)
			if (active[i])
				count++;
		return count;
	}
}