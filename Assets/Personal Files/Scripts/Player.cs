﻿using UnityEngine;

public class Player : MonoBehaviour
{
    private static float health;
    private static Inventory inventory;
    private static int selectedItem;


    public void Start()
    {
        health = Constant.MAX_HEALTH;
        selectedItem = -1;
        inventory = GetComponent<Inventory>();
    }

    public static void addItem(Item i)
    {
        inventory.addItem(i);
    }

    public static int getFreeInventorySlot()
    {
        return inventory.getFreeSlot();
    }

    public static void removeItem(int i)
    {
        inventory.remove(i);
    }

    public static float getHealth()
    {
        return health;
    }

    public static bool reduceHealth(float amount)
    {
        health -= amount; return (health > 0); // returns boolean result whether player is alive after the health reduction
    }

    public static void selectItem(int i)
    {
        if (i >= -1 && i < Constant.INVENTORY_SIZE)
            selectedItem = i;
        else
            selectedItem = -1;
    }

    public static void dealDamage(float dmg, int damageType)
    {
        if (selectedItem == -1)
        {
            reduceHealth(dmg);
        }
        else
        {
            reduceHealth(dmg - inventory.items[selectedItem].calculateDamageReduction(dmg, damageType));
            removeItem(selectedItem);
            selectItem(-1);
        }
    }

    public static bool alive()
    {
        return (health > 0);
    }

    public static void onDeath()
    {
        // TODO: implement, trigger, do sth;
    }
    public void OnMove(int side, Level level)
    {
        Debug.Log("moving to"+side);
        if (side == 0)
        {
            Item item = level.roomLeft.item;
            if (item != null)
            {
                inventory.addItem(item);
            }
        }
        else
        {
            Item item = level.roomRight.item;
            if (item != null)
            {
                inventory.addItem(item);
            }
        }
    }
}