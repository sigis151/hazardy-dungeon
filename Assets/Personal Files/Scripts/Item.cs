﻿/*  DAMAGE TYPES
 *  0: fire
 *  1: poison
 *  2: pierce
 *  3: blunt
 *  4: fall
 *  5: drown
 *  6: shock
 *  DON'T MESS UP THEIR ORDER!!!
 */
using UnityEngine;
public class Item : MonoBehaviour
{
    private string name;
	public float[] resistance;

    public Item (string itemName, float[] resistanceValues)
    {
        this.name = itemName;
        this.resistance = resistanceValues;
    }

    public void Set(string itemName, float[] resistanceValues)
    {
        this.name = itemName;
        resistance = resistanceValues;
    }

    public float calculateDamageReduction(float damage, int damageType) // reduction is supposed to be subtracted from total damage
    {
        float reduction = resistance[damageType] * damage;

        return reduction;
    }

    public string getName()
    {
        return this.name;
    }

}