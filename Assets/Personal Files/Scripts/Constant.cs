﻿using UnityEngine;
using System.Collections;

public static class Constant {
    public static void doNothing()
    { }

	public static int MAX_HEALTH = 1000;
	public static int INVENTORY_SIZE = 4;
    public static int ITEM_COUNT = 12;
    public static int ROOM_COUNT = 13;
    public static int MIN_ROOM_POOL_ITEMS = 5;
}
