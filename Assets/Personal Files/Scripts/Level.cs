﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour
{
    public Player player;


    public Room roomLeft { get; private set; }
    public Room roomRight { get; private set; }
    public GameObject LeftMountPoint;
    public GameObject RightMountPOint;
    public delegate void RoomSelectObserver(int side);
    public event RoomSelectObserver roomSelectObserver;
    public void Start()
    {
        roomLeft = RoomSelectionPool.getRandom(LeftMountPoint);
        roomRight = RoomSelectionPool.getRandom(RightMountPOint);
        RoomSelectionPool.resetLastPicked();
        //int rand1 = UnityEngine.Random.Range(0, Constant.ITEM_COUNT);
        //int rand2 = UnityEngine.Random.Range(0, Constant.ITEM_COUNT);
        //placeItem(ItemFactory.createItem(rand1));
        // placeItem(ItemFactory.createItem(rand2));

    }

    public void assignRoom(Room r)
    {
        if (roomLeft == null)
            roomLeft = r;
        else if (roomRight == null)
            roomRight = r;
        // TODO: randomize whether left or right is assigned first when picking from the pool.
    }

    public void resetRooms()
    {
        Destroy(roomLeft);
        Destroy(roomRight);
    }

    /*
        public void reload()
        {
            resetItems();
            resetRooms();
            assignRoom(RoomSelectionPool.getRandom());
            assignRoom(RoomSelectionPool.getRandom());
            RoomSelectionPool.resetLastPicked();
            int rand1 = UnityEngine.Random.Range(0, Constant.ITEM_COUNT);
            int rand2 = UnityEngine.Random.Range(0, Constant.ITEM_COUNT);
            placeItem(ItemFactory.createItem(rand1));
            placeItem(ItemFactory.createItem(rand2));
        }
*/
    public void goThrough(bool side)
    {
        /*     if (side)
             {
                 Player.dealDamage(roomRight.getAbsoluteDamage(), roomRight.getDamageType());
                 Player.addItem(pickUp(side));
             }
             else
             {
                 Player.dealDamage(roomLeft.getAbsoluteDamage(), roomLeft.getDamageType());
                 Player.addItem(pickUp(side));
             }
             if (Player.alive ()) 
             {
                 //reload();
             }
             else
                 Constant.doNothing(); // TODO: implement game-over action here*/
    }


    // Update is called once per frame
    void Update()
    {
        DetectTouch();
    }
    /// <summary>
    /// Detects the side on which you tapped.
    /// </summary>
    public void DetectTouch()
    {
        if (Input.GetMouseButtonDown(0) && Input.mousePosition.y > (Screen.width / Constant.INVENTORY_SIZE))
        {
            if (Input.mousePosition.x < Screen.width / 2)
            {
                player.OnMove(0, this);
                RoomSelectObserver handler = roomSelectObserver;
                if (handler != null)
                {
                    handler(0);
                }
            }
            else
            {
                player.OnMove(1, this);
                RoomSelectObserver handler = roomSelectObserver;
                if (handler != null)
                {
                    handler(1);
                }
            }
        }
    }
}
