﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InventoryInterface : MonoBehaviour {
	public GameObject frame;
	public Inventory inventory;
	void Start () {
		inventory = GameObject.FindWithTag ("Player").GetComponent<Inventory>();
		inventory.OnInventoryChanged += OnInventoryChanged;
		createSlots ();
	}

	void createSlots()
	{
		int size = Screen.width / Constant.INVENTORY_SIZE;
		Vector2 sizeVector = new Vector2(size, size);
		for (int i = 0; i < Constant.INVENTORY_SIZE; i++) {
			Vector2 pos = new Vector2(i*size, size);
			GameObject temp = Instantiate (frame, pos, Quaternion.identity) as GameObject;
            temp.name = "Slot_"+i;
			temp.GetComponent<RectTransform>().sizeDelta = sizeVector;
			temp.transform.SetParent (gameObject.transform);
		}
	}

	void OnInventoryChanged(){

	}
}
