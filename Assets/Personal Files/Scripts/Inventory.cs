﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour{
	public Item[] items{get; private set;}
	Inventory(){
		items = new Item[Constant.INVENTORY_SIZE];
	}
	public delegate void inventory ();
	public event inventory OnInventoryChanged;
	/// <summary>
	/// Gets the free slot index.
	/// </summary>
	/// <returns>The free slot.</returns>
	public int getFreeSlot() {
		for (int i = 0; i < items.Length; i++) {
			if(items[i] == null){
				return i;
			}
		}
		return -1;
	}
	/// <summary>
	/// Adds the item.
	/// </summary>
	/// <param name="item">Item.</param>
	/// <param name="index">Index.</param>
	public void addItem(Item item) {
		items [getFreeSlot()] = item;
        item.transform.SetParent(gameObject.transform);
        Debug.Log("adding item to empty slot");
		inventory handler = OnInventoryChanged;
		if (handler!= null) {
			handler ();
		}
	}
	/// <summary>
	/// Remove the specified index.
	/// </summary>
	/// <param name="index">Index.</param>
	public void remove(int index) {
		items [index] = null;
		inventory handler = OnInventoryChanged;
		if (handler != null) {
			handler ();
		}
	}
}

